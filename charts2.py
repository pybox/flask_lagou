#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
import os.path
from collections import Counter

from models import Lagou



# rule_unicode = re.compile(r"(?=k)[\u4E00-\u9FA5]+")
rule_gbk = re.compile(r"[k\x80-\xff]+")
_map = {u"1-3": u"1-3年", u"3-5": u"3-5年", u"5-10": u"5-10年", u"不限年": u"不限",
        u"1年以下年": u"1年以下", u"无经验年": u"应届毕业生", u"无经验": u"应届毕业生"}


def save_in_js(_json, _name, _js=True, _html=True):
    basedir = os.path.abspath(os.path.dirname(__file__))
    prefix = "unit_"

    if _js:
        suffix = ".js"
        with open(os.path.join(basedir, "static", "alls", "js", _name + suffix), "wb") as _wf:
            json.dump(_json, _wf, indent=4, sort_keys=True)

    if _html:
        suffix = ".html"
        with open(os.path.join(basedir, "templates", prefix + _name + suffix), "wb") as _wf:
            _wf.write("{% extends 'baseChart.html' %}\n")
            _wf.write("{%% block description %%}%s{%% endblock %%}\n" % _name)
            _wf.write("{%% set chart_name = '%s' %%}\n" % _name)

    return


def db_datas():
    _page_size = 20000
    _sum = Lagou.query.count()
    _max = _sum % _page_size == 0 and _sum / _page_size or _sum / _page_size + 1

    for _page in xrange(_max):
        data = Lagou.query.offset(_page_size * _page).limit(_page_size).all()
        yield data


def _stat_func(counterObj):
    # ["Name":,"Mode", "Median"]

    result, _dc = [], {}
    for k, v in counterObj.items():
        _string = k[0]
        _dc[_string] = [k[1]] * v + _dc.get(_string, [])

    for kw, _list in _dc.items():
        if _list:
            _list.sort()

            cObj, length = Counter(_list), len(_list)
            _mode = cObj.most_common(1)
            _median = length % 2 == 1 and _list[length / 2] or (_list[length / 2 - 1] + _list[length / 2]) / 2.0
            _ = {"Name": kw, "Mode": _mode, "Median": _median}
            result.append(_)
    return result


def _salary_func(keyword):
    _result = []
    for frags in db_datas():
        for item in frags:
            _data = getattr(item, keyword)
            _k, _v = _map.get(_data) or _data, item.salary

            # condition modify
            if keyword == "industryField":
                _k = _k.split(u"·")[0].strip()

            _text = re.sub(rule_gbk, "", _v.encode("gbk"))
            _text = _text.split("-")
            _money = [int(str(_)) for _ in _text]
            _v = sum(_money) * 0.5 * 1000 if len(_text) == 2 else int(_text[0]) * 1000

            _result.append((_k, _v))

    rC = Counter(_result)
    rD = {}
    for k, v in rC.items():
        _k = k[0]

        # condition modify
        if keyword == "city":
            _k = len(list(_k)) == 3 and "".join(list(_k)[:2]) or _k  # 3转2

        if _k not in rD.keys():
            rD[_k] = {"salary": k[1], "count": v}
        else:
            _ = rD[_k]
            _salary, _count = _["salary"] * _["count"] + k[1] * v, _["count"] + v
            _average = round(_salary / _count, 2)
            rD.update({_k: {"salary": _average, "count": _count}})

    stat = _stat_func(rC)
    return rD, stat


def unit_experience_salary(isJson=False, isJs=False, isHtml=False):
    """经验——薪资"""
    result, stat = _salary_func(keyword="workYear")

    jR = {
        "datas": [{"count": v["count"], "name": k, "salary": v["salary"]} for k, v in result.items() if k],
        "stat": stat,
        "titlo": u"经验——薪资",
        "c_type": "column",
        "c_field": "salary",
    }

    if isJson:
        return jR

    if isJs:
        # 经验——薪资
        save_in_js(jR, "experience_salary")

        # 经验——职位数
        _ = {
            "titlo": u"经验——职位数",
            "c_field": "count",
            "c_type": "pie",
        }
        jR.update(_);jR.__delitem__("stat")
        save_in_js(jR, "experience_posts")
        return


def unit_job_salary(isJson=False, isJs=False, isHtml=False):
    """工作类型——薪资"""

    result, stat = _salary_func(keyword="positionFirstType")
    jR = {
        "datas": [{"count": v["count"], "name": k, "salary": v["salary"]} for k, v in result.items() if k],
        "stat": stat,
        "titlo": u"工作类型——薪资",
        "c_type": "column",
        "c_field": "salary",
    }

    if isJson:
        return jR

    if isJs:
        # 工作类型——薪资
        save_in_js(jR, "job_salary")

        # 工作类型——职位数
        _ = {
            "titlo": u"工作类型——职位数",
            "c_field": "count",
            "c_type": "pie",
        }
        jR.update(_);jR.__delitem__("stat")
        save_in_js(jR, "job_posts")
        return


def unit_city_salary(isJson=False, isJs=False, isHtml=False):
    """城市——薪资"""
    result, stat = _salary_func(keyword="city")
    stat = {item["Name"]: item for item in stat}

    result_counts = [item["count"] for item in result.values()]
    _all = sum(result_counts) * 0.01

    jR = {
        "datas": [{"count": v["count"], "name": k, "salary": v["salary"]}
                  for k, v in result.items() if k if (v["count"] > _all)],
        "stat": [stat[k] for k, v in result.items() if k if (v["count"] > _all)],
        "titlo": u"城市——薪资(仅显示占总样本数量>1%的城市)",
        "c_type": "column",
        "c_field": "salary",
    }

    if isJs:
        # 城市——薪资
        save_in_js(jR, "city_salary")

        # 城市——职位数
        _all = sum(result_counts) * 0.03
        jR = {
            "datas": [{"count": v["count"], "name": k} for k, v in result.items() if k if (v["count"] > _all)] +
                     [{"count": sum([i for i in result_counts if (i <= _all)]), "name": u"其他"}],
            "titlo": u"城市——职位数",
            "c_field": "count",
            "c_type": "pie",
        }
        save_in_js(jR, "city_posts")
        return


def unit_company_size_salary(isJson=False, isJs=False, isHtml=False):
    """公司规模——薪资"""
    result, stat = _salary_func(keyword="companySize")
    jR = {
        "datas": [{"count": v["count"], "name": k, "salary": v["salary"]} for k, v in result.items() if k],
        "stat": stat,
        "titlo": u"公司规模——薪资",
        "c_type": "column",
        "c_field": "salary",
    }

    if isJson:
        return jR

    if isJs:
        # 公司规模——薪资
        save_in_js(jR, "company_size_salary")

        # 公司规模——职位数
        _ = {
            "titlo": u"公司规模——职位数",
            "c_field": "count",
            "c_type": "pie",
        }
        jR.update(_);jR.__delitem__("stat")
        save_in_js(jR, "company_size_posts")
        return


def unit_category_salary(isJson=False, isJs=False, isHtml=False):
    """行业分类——薪资"""
    result, stat = _salary_func(keyword="industryField")
    jR = {
        "datas": [{"count": v["count"], "name": k, "salary": v["salary"]} for k, v in result.items() if k],
        "stat": stat,
        "titlo": u"行业分类——薪资",
        # "c_type": "column",
        "c_field": "salary",
    }

    if isJson:
        return jR

    if isJs:
        # 行业分类——薪资
        save_in_js(jR, "category_salary")

        # 行业分类——职位数
        result_counts = [item["count"] for item in result.values()]
        _all = sum(result_counts) * 0.03
        jR = {
            "datas": [{"count": v["count"], "name": k} for k, v in result.items() if k if (v["count"] > _all)] +
                     [{"count": sum([i for i in result_counts if (i <= _all)]), "name": u"其他"}],
            "titlo": u"行业分类——职位数(仅显示占总样本数量>3%的城市)",
            "c_type": "pie",
        }
        save_in_js(jR, "category_posts")
        return


if __name__ == '__main__':
    map(lambda func: eval(func)(isJs=True), [f for f in dir() if "unit" in f])
