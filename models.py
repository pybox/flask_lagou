#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sqlalchemy import (Column, Integer, String, DateTime, ForeignKey, create_engine, PrimaryKeyConstraint)
from sqlalchemy.orm import (scoped_session, sessionmaker)
from sqlalchemy.ext.declarative import declarative_base

SQLALCHEMY_DATABASE_URI = r"sqlite:///E:\Bitbucket\flask_lagou\db\lagou.db"

engine = create_engine(SQLALCHEMY_DATABASE_URI, convert_unicode=True, echo=False)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


class Lagou(Base):
    __tablename__ = "datas"
    __table_args__ = (
        PrimaryKeyConstraint('positionId', "companyId"),
    )

    adWord = Column(String)
    adjustScore = Column(String)
    appShow = Column(String)
    approve = Column(String)
    businessZones = Column(String)
    calcScore = Column(String)
    city = Column(String)
    companyId = Column(String)
    companyLabelList = Column(String)
    companyLogo = Column(String)
    companyName = Column(String)
    companyShortName = Column(String)
    companySize = Column(String)
    countAdjusted = Column(String)
    createTime = Column(String)
    createTimeSort = Column(String)
    deliverCount = Column(String)
    district = Column(String)
    education = Column(String)
    financeStage = Column(String)
    flowScore = Column(String)
    formatCreateTime = Column(String)
    haveDeliver = Column(String)
    hrScore = Column(String)
    imstate = Column(String)
    industryField = Column(String)
    jobNature = Column(String)
    leaderName = Column(String)
    loginTime = Column(String)
    orderBy = Column(String)
    plus = Column(String)
    positionAdvantage = Column(String)
    positionFirstType = Column(String)
    positionId = Column(String)
    positionName = Column(String)
    positionType = Column(String)
    positonTypesMap = Column(String)
    publisherId = Column(String)
    pvScore = Column(String)
    randomScore = Column(String)
    relScore = Column(String)
    salary = Column(String)
    score = Column(String)
    searchScore = Column(String)
    showCount = Column(String)
    showOrder = Column(String)
    totalCount = Column(String)
    workYear = Column(String)

    def __repr__(self):
        return '<lagouObject positionId: %s>' % self.positionId

    def as_dict(self):
        return self.__dict__
