{
    "c_field": "salary", 
    "datas": [
        {
            "count": 6966, 
            "name": "\u6570\u636e\u670d\u52a1", 
            "salary": 13065.77
        }, 
        {
            "count": 2855, 
            "name": "\u5e7f\u544a\u8425\u9500", 
            "salary": 9198.08
        }, 
        {
            "count": 4513, 
            "name": "\u5176\u4ed6", 
            "salary": 9626.62
        }, 
        {
            "count": 134315, 
            "name": "\u79fb\u52a8\u4e92\u8054\u7f51", 
            "salary": 12076.38
        }, 
        {
            "count": 521, 
            "name": "\u62db\u8058", 
            "salary": 13861.81
        }, 
        {
            "count": 8937, 
            "name": "O2O", 
            "salary": 10994.1
        }, 
        {
            "count": 5, 
            "name": "\u8996\u983b\u591a\u5a92\u9ad4", 
            "salary": 13500.0
        }, 
        {
            "count": 2407, 
            "name": "\u751f\u6d3b\u670d\u52a1", 
            "salary": 9488.15
        }, 
        {
            "count": 5, 
            "name": "\u793e\u4ea4", 
            "salary": 21300.0
        }, 
        {
            "count": 189, 
            "name": "\u5206\u7c7b\u4fe1\u606f", 
            "salary": 9158.73
        }, 
        {
            "count": 1, 
            "name": "\u5927\u6570\u636e", 
            "salary": 22500.0
        }, 
        {
            "count": 31020, 
            "name": "\u7535\u5b50\u5546\u52a1", 
            "salary": 10867.12
        }, 
        {
            "count": 5763, 
            "name": "\u6e38\u620f", 
            "salary": 11275.81
        }, 
        {
            "count": 3619, 
            "name": "\u6587\u5316\u5a31\u4e50", 
            "salary": 9542.13
        }, 
        {
            "count": 1705, 
            "name": "\u4fe1\u606f\u5b89\u5168", 
            "salary": 10445.17
        }, 
        {
            "count": 3640, 
            "name": "\u786c\u4ef6", 
            "salary": 12828.02
        }, 
        {
            "count": 1413, 
            "name": "\u65c5\u6e38", 
            "salary": 9883.93
        }, 
        {
            "count": 2548, 
            "name": "\u533b\u7597\u5065\u5eb7", 
            "salary": 11481.73
        }, 
        {
            "count": 11098, 
            "name": "\u4f01\u4e1a\u670d\u52a1", 
            "salary": 11186.61
        }, 
        {
            "count": 1734, 
            "name": "\u793e\u4ea4\u7f51\u7edc", 
            "salary": 12227.81
        }, 
        {
            "count": 6205, 
            "name": "\u6559\u80b2", 
            "salary": 9349.39
        }, 
        {
            "count": 18769, 
            "name": "\u91d1\u878d", 
            "salary": 12073.42
        }
    ], 
    "stat": [
        {
            "Median": 9250.0, 
            "Mode": [
                [
                    15000.0, 
                    2
                ]
            ], 
            "Name": ""
        }, 
        {
            "Median": 7000.0, 
            "Mode": [
                [
                    6000.0, 
                    294
                ]
            ], 
            "Name": "\u5e7f\u544a\u8425\u9500"
        }, 
        {
            "Median": 10000.0, 
            "Mode": [
                [
                    15000.0, 
                    11203
                ]
            ], 
            "Name": "\u79fb\u52a8\u4e92\u8054\u7f51"
        }, 
        {
            "Median": 8500.0, 
            "Mode": [
                [
                    60000.0, 
                    34
                ]
            ], 
            "Name": "\u62db\u8058"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    282
                ]
            ], 
            "Name": "\u751f\u6d3b\u670d\u52a1"
        }, 
        {
            "Median": 22500.0, 
            "Mode": [
                [
                    30000.0, 
                    2
                ]
            ], 
            "Name": "\u793e\u4ea4"
        }, 
        {
            "Median": 10000.0, 
            "Mode": [
                [
                    15000.0, 
                    511
                ]
            ], 
            "Name": "\u6e38\u620f"
        }, 
        {
            "Median": 9000.0, 
            "Mode": [
                [
                    7500.0, 
                    157
                ]
            ], 
            "Name": "\u4fe1\u606f\u5b89\u5168"
        }, 
        {
            "Median": 10500.0, 
            "Mode": [
                [
                    15000.0, 
                    298
                ]
            ], 
            "Name": "\u786c\u4ef6"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    5000.0, 
                    149
                ]
            ], 
            "Name": "\u65c5\u6e38"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    5000.0, 
                    356
                ]
            ], 
            "Name": "\u5176\u4ed6"
        }, 
        {
            "Median": 10500.0, 
            "Mode": [
                [
                    15000.0, 
                    132
                ]
            ], 
            "Name": "\u793e\u4ea4\u7f51\u7edc"
        }, 
        {
            "Median": 10000.0, 
            "Mode": [
                [
                    15000.0, 
                    1531
                ]
            ], 
            "Name": "\u91d1\u878d"
        }, 
        {
            "Median": 11500.0, 
            "Mode": [
                [
                    15000.0, 
                    658
                ]
            ], 
            "Name": "\u6570\u636e\u670d\u52a1"
        }, 
        {
            "Median": 9000.0, 
            "Mode": [
                [
                    9000.0, 
                    710
                ]
            ], 
            "Name": "O2O"
        }, 
        {
            "Median": 11500.0, 
            "Mode": [
                [
                    11500.0, 
                    3
                ]
            ], 
            "Name": "\u8996\u983b\u591a\u5a92\u9ad4"
        }, 
        {
            "Median": 7000.0, 
            "Mode": [
                [
                    4000.0, 
                    19
                ]
            ], 
            "Name": "\u5206\u7c7b\u4fe1\u606f"
        }, 
        {
            "Median": 22500.0, 
            "Mode": [
                [
                    22500.0, 
                    1
                ]
            ], 
            "Name": "\u5927\u6570\u636e"
        }, 
        {
            "Median": 8500.0, 
            "Mode": [
                [
                    6000.0, 
                    2298
                ]
            ], 
            "Name": "\u7535\u5b50\u5546\u52a1"
        }, 
        {
            "Median": 9000.0, 
            "Mode": [
                [
                    15000.0, 
                    270
                ]
            ], 
            "Name": "\u533b\u7597\u5065\u5eb7"
        }, 
        {
            "Median": 9000.0, 
            "Mode": [
                [
                    15000.0, 
                    891
                ]
            ], 
            "Name": "\u4f01\u4e1a\u670d\u52a1"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    5000.0, 
                    485
                ]
            ], 
            "Name": "\u6559\u80b2"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    302
                ]
            ], 
            "Name": "\u6587\u5316\u5a31\u4e50"
        }
    ], 
    "titlo": "\u884c\u4e1a\u5206\u7c7b\u2014\u2014\u85aa\u8d44"
}