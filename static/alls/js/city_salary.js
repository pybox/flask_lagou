{
    "c_field": "salary", 
    "c_type": "column", 
    "datas": [
        {
            "count": 87484, 
            "name": "\u5317\u4eac", 
            "salary": 13445.88
        }, 
        {
            "count": 2577, 
            "name": "\u897f\u5b89", 
            "salary": 7243.31
        }, 
        {
            "count": 2739, 
            "name": "\u53a6\u95e8", 
            "salary": 8551.27
        }, 
        {
            "count": 33879, 
            "name": "\u6df1\u5733", 
            "salary": 12025.3
        }, 
        {
            "count": 7957, 
            "name": "\u6210\u90fd", 
            "salary": 8676.55
        }, 
        {
            "count": 42217, 
            "name": "\u4e0a\u6d77", 
            "salary": 12536.69
        }, 
        {
            "count": 4343, 
            "name": "\u6b66\u6c49", 
            "salary": 8157.26
        }, 
        {
            "count": 22970, 
            "name": "\u5e7f\u5dde", 
            "salary": 9143.06
        }, 
        {
            "count": 18679, 
            "name": "\u676d\u5dde", 
            "salary": 11476.01
        }, 
        {
            "count": 3942, 
            "name": "\u5357\u4eac", 
            "salary": 9706.49
        }
    ], 
    "stat": [
        {
            "Median": 11500.0, 
            "Mode": [
                [
                    15000.0, 
                    7709
                ]
            ], 
            "Name": "\u5317\u4eac"
        }, 
        {
            "Median": 6500.0, 
            "Mode": [
                [
                    6000.0, 
                    248
                ]
            ], 
            "Name": "\u897f\u5b89"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    7500.0, 
                    251
                ]
            ], 
            "Name": "\u53a6\u95e8"
        }, 
        {
            "Median": 10000.0, 
            "Mode": [
                [
                    15000.0, 
                    3070
                ]
            ], 
            "Name": "\u6df1\u5733"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    663
                ]
            ], 
            "Name": "\u6210\u90fd"
        }, 
        {
            "Median": 11500.0, 
            "Mode": [
                [
                    15000.0, 
                    3982
                ]
            ], 
            "Name": "\u4e0a\u6d77"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    445
                ]
            ], 
            "Name": "\u6b66\u6c49"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    1851
                ]
            ], 
            "Name": "\u5e7f\u5dde"
        }, 
        {
            "Median": 10000.0, 
            "Mode": [
                [
                    15000.0, 
                    1765
                ]
            ], 
            "Name": "\u676d\u5dde"
        }, 
        {
            "Median": 8000.0, 
            "Mode": [
                [
                    6000.0, 
                    317
                ]
            ], 
            "Name": "\u5357\u4eac"
        }
    ], 
    "titlo": "\u57ce\u5e02\u2014\u2014\u85aa\u8d44(\u4ec5\u663e\u793a\u5360\u603b\u6837\u672c\u6570\u91cf>1%\u7684\u57ce\u5e02)"
}