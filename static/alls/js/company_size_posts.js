{
    "c_field": "count", 
    "c_type": "pie", 
    "datas": [
        {
            "count": 56152, 
            "name": "15-50\u4eba", 
            "salary": 10237.62
        }, 
        {
            "count": 30525, 
            "name": "2000\u4eba\u4ee5\u4e0a", 
            "salary": 14241.58
        }, 
        {
            "count": 61703, 
            "name": "50-150\u4eba", 
            "salary": 11078.48
        }, 
        {
            "count": 11492, 
            "name": "\u5c11\u4e8e15\u4eba", 
            "salary": 9359.67
        }, 
        {
            "count": 53937, 
            "name": "150-500\u4eba", 
            "salary": 12059.79
        }, 
        {
            "count": 34419, 
            "name": "500-2000\u4eba", 
            "salary": 12670.79
        }
    ], 
    "titlo": "\u516c\u53f8\u89c4\u6a21\u2014\u2014\u804c\u4f4d\u6570"
}