{
    "c_field": "salary", 
    "c_type": "column", 
    "datas": [
        {
            "count": 56152, 
            "name": "15-50\u4eba", 
            "salary": 10237.62
        }, 
        {
            "count": 30525, 
            "name": "2000\u4eba\u4ee5\u4e0a", 
            "salary": 14241.58
        }, 
        {
            "count": 61703, 
            "name": "50-150\u4eba", 
            "salary": 11078.48
        }, 
        {
            "count": 11492, 
            "name": "\u5c11\u4e8e15\u4eba", 
            "salary": 9359.67
        }, 
        {
            "count": 53937, 
            "name": "150-500\u4eba", 
            "salary": 12059.79
        }, 
        {
            "count": 34419, 
            "name": "500-2000\u4eba", 
            "salary": 12670.79
        }
    ], 
    "stat": [
        {
            "Median": 9250.0, 
            "Mode": [
                [
                    15000.0, 
                    2
                ]
            ], 
            "Name": ""
        }, 
        {
            "Median": 8000.0, 
            "Mode": [
                [
                    6000.0, 
                    4287
                ]
            ], 
            "Name": "15-50\u4eba"
        }, 
        {
            "Median": 12500.0, 
            "Mode": [
                [
                    22500.0, 
                    2459
                ]
            ], 
            "Name": "2000\u4eba\u4ee5\u4e0a"
        }, 
        {
            "Median": 10000.0, 
            "Mode": [
                [
                    15000.0, 
                    4579
                ]
            ], 
            "Name": "150-500\u4eba"
        }, 
        {
            "Median": 9000.0, 
            "Mode": [
                [
                    15000.0, 
                    4601
                ]
            ], 
            "Name": "50-150\u4eba"
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    976
                ]
            ], 
            "Name": "\u5c11\u4e8e15\u4eba"
        }, 
        {
            "Median": 11500.0, 
            "Mode": [
                [
                    15000.0, 
                    3197
                ]
            ], 
            "Name": "500-2000\u4eba"
        }
    ], 
    "titlo": "\u516c\u53f8\u89c4\u6a21\u2014\u2014\u85aa\u8d44"
}