{
    "c_field": "count", 
    "c_type": "pie", 
    "datas": [
        {
            "count": 101379, 
            "name": "1-3\u5e74", 
            "salary": 9611.27
        }, 
        {
            "count": 8671, 
            "name": "\u5e94\u5c4a\u6bd5\u4e1a\u751f", 
            "salary": 5425.75
        }, 
        {
            "count": 66669, 
            "name": "3-5\u5e74", 
            "salary": 15736.83
        }, 
        {
            "count": 47538, 
            "name": "\u4e0d\u9650", 
            "salary": 7574.39
        }, 
        {
            "count": 704, 
            "name": "10\u5e74\u4ee5\u4e0a", 
            "salary": 34897.74
        }, 
        {
            "count": 17821, 
            "name": "5-10\u5e74", 
            "salary": 22384.52
        }, 
        {
            "count": 5452, 
            "name": "1\u5e74\u4ee5\u4e0b", 
            "salary": 6094.19
        }
    ], 
    "titlo": "\u7ecf\u9a8c\u2014\u2014\u804c\u4f4d\u6570"
}