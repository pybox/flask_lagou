{
    "c_field": "salary", 
    "c_type": "column", 
    "datas": [
        {
            "count": 101379, 
            "name": "1-3\u5e74", 
            "salary": 9611.27
        }, 
        {
            "count": 8671, 
            "name": "\u5e94\u5c4a\u6bd5\u4e1a\u751f", 
            "salary": 5425.75
        }, 
        {
            "count": 66669, 
            "name": "3-5\u5e74", 
            "salary": 15736.83
        }, 
        {
            "count": 47538, 
            "name": "\u4e0d\u9650", 
            "salary": 7574.39
        }, 
        {
            "count": 704, 
            "name": "10\u5e74\u4ee5\u4e0a", 
            "salary": 34897.74
        }, 
        {
            "count": 17821, 
            "name": "5-10\u5e74", 
            "salary": 22384.52
        }, 
        {
            "count": 5452, 
            "name": "1\u5e74\u4ee5\u4e0b", 
            "salary": 6094.19
        }
    ], 
    "stat": [
        {
            "Median": 8000.0, 
            "Mode": [
                [
                    7500.0, 
                    8479
                ]
            ], 
            "Name": "1-3\u5e74"
        }, 
        {
            "Median": 5000.0, 
            "Mode": [
                [
                    4000.0, 
                    1039
                ]
            ], 
            "Name": "\u5e94\u5c4a\u6bd5\u4e1a\u751f"
        }, 
        {
            "Median": 15000.0, 
            "Mode": [
                [
                    15000.0, 
                    9085
                ]
            ], 
            "Name": "3-5\u5e74"
        }, 
        {
            "Median": 6000.0, 
            "Mode": [
                [
                    6000.0, 
                    5351
                ]
            ], 
            "Name": "\u4e0d\u9650"
        }, 
        {
            "Median": 30000.0, 
            "Mode": [
                [
                    30000.0, 
                    82
                ]
            ], 
            "Name": "10\u5e74\u4ee5\u4e0a"
        }, 
        {
            "Median": 21500.0, 
            "Mode": [
                [
                    22500.0, 
                    1973
                ]
            ], 
            "Name": "5-10\u5e74"
        }, 
        {
            "Median": 5500.0, 
            "Mode": [
                [
                    5000.0, 
                    734
                ]
            ], 
            "Name": "1\u5e74\u4ee5\u4e0b"
        }
    ], 
    "titlo": "\u7ecf\u9a8c\u2014\u2014\u85aa\u8d44"
}