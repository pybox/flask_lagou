{
    "c_field": "count", 
    "c_type": "pie", 
    "datas": [
        {
            "count": 50420, 
            "name": "\u5e02\u573a\u4e0e\u9500\u552e", 
            "salary": 8991.9
        }, 
        {
            "count": 104293, 
            "name": "\u6280\u672f", 
            "salary": 14537.88
        }, 
        {
            "count": 14878, 
            "name": "\u4ea7\u54c1", 
            "salary": 15587.18
        }, 
        {
            "count": 41561, 
            "name": "\u8fd0\u8425", 
            "salary": 8171.05
        }, 
        {
            "count": 17147, 
            "name": "\u8bbe\u8ba1", 
            "salary": 9912.14
        }, 
        {
            "count": 15580, 
            "name": "\u804c\u80fd", 
            "salary": 7853.04
        }, 
        {
            "count": 4213, 
            "name": "\u91d1\u878d", 
            "salary": 12465.61
        }
    ], 
    "titlo": "\u5de5\u4f5c\u7c7b\u578b\u2014\u2014\u804c\u4f4d\u6570"
}