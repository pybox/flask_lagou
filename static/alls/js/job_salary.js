{
    "c_field": "salary", 
    "c_type": "column", 
    "datas": [
        {
            "count": 50420, 
            "name": "\u5e02\u573a\u4e0e\u9500\u552e", 
            "salary": 8991.9
        }, 
        {
            "count": 104293, 
            "name": "\u6280\u672f", 
            "salary": 14537.88
        }, 
        {
            "count": 14878, 
            "name": "\u4ea7\u54c1", 
            "salary": 15587.18
        }, 
        {
            "count": 41561, 
            "name": "\u8fd0\u8425", 
            "salary": 8171.05
        }, 
        {
            "count": 17147, 
            "name": "\u8bbe\u8ba1", 
            "salary": 9912.14
        }, 
        {
            "count": 15580, 
            "name": "\u804c\u80fd", 
            "salary": 7853.04
        }, 
        {
            "count": 4213, 
            "name": "\u91d1\u878d", 
            "salary": 12465.61
        }
    ], 
    "stat": [
        {
            "Median": 7750.0, 
            "Mode": [
                [
                    15000.0, 
                    14
                ]
            ], 
            "Name": ""
        }, 
        {
            "Median": 7500.0, 
            "Mode": [
                [
                    6000.0, 
                    5618
                ]
            ], 
            "Name": "\u5e02\u573a\u4e0e\u9500\u552e"
        }, 
        {
            "Median": 12500.0, 
            "Mode": [
                [
                    15000.0, 
                    11300
                ]
            ], 
            "Name": "\u6280\u672f"
        }, 
        {
            "Median": 15000.0, 
            "Mode": [
                [
                    15000.0, 
                    1812
                ]
            ], 
            "Name": "\u4ea7\u54c1"
        }, 
        {
            "Median": 6500.0, 
            "Mode": [
                [
                    5000.0, 
                    4106
                ]
            ], 
            "Name": "\u8fd0\u8425"
        }, 
        {
            "Median": 8500.0, 
            "Mode": [
                [
                    6000.0, 
                    1325
                ]
            ], 
            "Name": "\u8bbe\u8ba1"
        }, 
        {
            "Median": 6000.0, 
            "Mode": [
                [
                    5000.0, 
                    2115
                ]
            ], 
            "Name": "\u804c\u80fd"
        }, 
        {
            "Median": 9000.0, 
            "Mode": [
                [
                    9000.0, 
                    486
                ]
            ], 
            "Name": "\u91d1\u878d"
        }
    ], 
    "titlo": "\u5de5\u4f5c\u7c7b\u578b\u2014\u2014\u85aa\u8d44"
}