from flask import Flask, render_template, Response
from charts2 import *
app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/query')
def query():
    data = unit_job_salary(isJson=True)
    return Response(json.dumps(data), mimetype='application/json')


if __name__ == '__main__':
    app.run(host="0.0.0.0")
